using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EXCEL = Microsoft.Office.Interop.Excel;

namespace PiaExcel
{
    /// <summary>
    /// PiaRange对象
    /// </summary>
    public class PiaRange
    {
        private EXCEL.Range mRange;

        /// <summary>
        /// PiaRange构造函数
        /// </summary>
        /// <param name="range"></param>
        public PiaRange(EXCEL.Range range)
        {
            this.Range = range;
        }

        /// <summary>
        /// 获取原生态Range对象
        /// </summary>
        public EXCEL.Range Range
        {
            get
            {
                return mRange;
            }
            set
            {
                mRange = value;
            }
        }


        internal object[,] RangeArray
        {
            get;
            set;
        }

        /// <summary>
        /// 获取行数据
        /// </summary>
        /// <returns></returns>
        public object[,] RangeValue()
        {

            this.RangeArray = this.Range.get_Value();
            return this.RangeArray;

        }


        public IEnumerable<PiaRow> RowsValue()
        {
            this.RangeArray = this.Range.get_Value();
            PiaRow pw = new PiaRow();
            for (int r = 1; r <= this.RangeArray.GetUpperBound(0); r++)
            {
                pw.Row = new object[1, this.ColumnsCount];
                for (int c = 1; c <= this.RangeArray.GetUpperBound(1); c++)
                {
                    pw.Row[0, c - 1] = this.RangeArray[r, c];
                }
                yield return pw;
            }

        }

        /// <summary>
        /// 获取单元格列号
        /// </summary>
        public int PiaRangeColumn
        {
            get
            {
                return this.Range.Column;
            }

        }

        /// <summary>
        /// 获取单元格行号
        /// </summary>
        public int PiaRangeRow
        {
            get
            {
                return this.Range.Row;
            }

        }

        /// <summary>
        /// 统计行数
        /// </summary>
        public int RowsCount
        {
            get
            {
                return this.Range.Rows.Count;
            }
        }

        /// <summary>
        ///统计列数 
        /// </summary>
        public int ColumnsCount
        {
            get
            {
                return this.Range.Columns.Count;
            }
        }

        /// <summary>
        /// 返回包含源区域的最上方单元格行号
        /// </summary>
        /// <returns></returns>
        public int XlUpRow
        {
            get
            {
                return this.Range.End[EXCEL.XlDirection.xlUp].Row;
            }
        }
        /// <summary>
        /// 返回包含源区域的最下方单元格行号
        /// </summary>
        /// <returns></returns>
        public int XlDownRow
        {
            get
            {
                return this.Range.End[EXCEL.XlDirection.xlDown].Row;
            }
        }


        /// <summary>
        /// 返回包含源区域的最左端单元格列号
        /// </summary>
        /// <returns></returns>
        public int XlToLeftColumn
        {
            get
            {
                return this.Range.End[EXCEL.XlDirection.xlToLeft].Column;
            }
        }

        /// <summary>
        /// 返回包含源区域的最右端单元格列号
        /// </summary>
        /// <returns></returns>
        public int XlToRightColumn
        {
            get
            {
                return this.Range.End[EXCEL.XlDirection.xlToRight].Column;
            }
        }

    }
}
