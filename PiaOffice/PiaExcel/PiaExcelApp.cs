﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using EXCEL = Microsoft.Office.Interop.Excel;
using System.Diagnostics;


namespace PiaExcel
{
    /// <summary>
    /// EXCEL进程
    /// </summary>

    #region Excel进程
    internal class PiaExcelApp
    {
        /// <summary>
        /// 获取excelApp进程
        /// </summary>
        /// <param name="excelApplication">原生态Excel进程</param>
        public PiaExcelApp(EXCEL.Application excelApplication)
        {
            this.ExcelApplication = excelApplication;
        }

        /// <summary>
        /// 封装ExcelApplication
        /// </summary>
        public EXCEL.Application ExcelApplication
        {
            get;
            private set;
        }

        /// <summary>
        /// 先检查当前是否存在excel实例,有则获取,否则先创建
        /// </summary>
        /// <returns></returns>
        public static PiaExcelApp Create()
        {
            Microsoft.Office.Interop.Excel.Application instance = null;

            try
            {
                instance = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                instance = new Microsoft.Office.Interop.Excel.Application();
            }

            return new PiaExcelApp(instance);
        }
    }
    #endregion


}