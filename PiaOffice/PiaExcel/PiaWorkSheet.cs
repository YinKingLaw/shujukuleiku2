﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EXCEL = Microsoft.Office.Interop.Excel;

namespace PiaExcel
{
    /// <summary>
    /// 自定义WorkSheet对象
    /// </summary>
    public class PiaWorkSheet
    {

        private EXCEL.Worksheet mWorkSheet;

        /// <summary>
        /// PiaWorkSheet构造函数
        /// </summary>
        /// <param name="worksheet"></param>
        public PiaWorkSheet(EXCEL.Worksheet worksheet)
        {
            mWorkSheet = worksheet;
        }
        /// <summary>
        /// 原生态WorkSheet对象
        /// </summary>
        public EXCEL.Worksheet WorkSheet
        {
            get
            {
                return mWorkSheet;
            }
            internal set
            {
                mWorkSheet = value;
            }
        }
        /// <summary>
        /// 获取PiaRange对象
        /// </summary>
        /// <param name="cell1">单元格地址</param>
        /// <returns></returns>
        public PiaRange Range(object cell1)
        {
            return this.Range(cell1, Type.Missing);
        }
        /// <summary>
        /// 获取PiaRange对象
        /// </summary>
        /// <param name="cell1">单元格1</param>
        /// <param name="cell2">单元格2</param>
        /// <returns></returns>
        public PiaRange Range(object cell1, object cell2)
        {
            PiaRange mRang = new PiaRange(this.WorkSheet.Range[cell1, cell2]);

            return mRang;
        }

    }
}
