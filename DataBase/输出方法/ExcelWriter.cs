﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EXCEL = Microsoft.Office.Interop.Excel;
using DataBase;
using PiaExcel;


namespace DataBase
{
    /// <summary>
    /// Excel输出类
    /// </summary>
    public static class ExcelWriter
    {

        /// <summary>
        /// 分批输出
        /// </summary>
        /// <param name="excelWorkSheet">PiaWorkSheet对象</param>
        /// <param name="isTableHeard">是否输出表头</param>
        /// <param name="outPutRow">输出行号</param>
        /// <param name="outPutColumn">输出列号</param>
        /// <param name="batchData">批次对象</param>
        public static void WriteToExcel(this PiaWorkSheet excelWorkSheet, bool isTableHeard, int outPutRow, int outPutColumn, Batch batchData)
        {

            OutPutWorkSheet = excelWorkSheet.WorkSheet;
            OutPutRow = outPutRow;
            OutPutColumn = outPutColumn;
            //一维数组转二维数组
            PiaDataDope pdd = new PiaDataDope();
            pdd.BatchToObjectArray(batchData);
            foreach (var item in pdd.BatchToObjectArray(batchData))
            {
                //输出表头
                if (isTableHeard)
                {
                    WriteTabeleHader();
                }
                EXCEL.Range rng = OutPutWorkSheet.Cells[OutPutRow, OutPutColumn];
                rng = rng.get_Resize(batchData.RowsList.Count, batchData.RowsList[0].Length);
                rng.Value = item;
            }

        }

        /// <summary>
        /// 全量输出EXCEL
        /// </summary>
        /// <param name="excelWorkSheet"></param>
        /// <param name="isTableHeard">是否输出表头</param>
        /// <param name="outPutRow">输出行号</param>
        /// <param name="outPutColumn">输出列号</param>
        /// <param name="rowList">行数据</param>
        public static void WriteToExcel(this PiaWorkSheet excelWorkSheet, bool isTableHeard, int outPutRow, int outPutColumn, IEnumerable<OracleRow> rowList)
        {
            OutPutWorkSheet = excelWorkSheet.WorkSheet;
            OutPutRow = outPutRow;
            OutPutColumn = outPutColumn;
            PiaDataDope pdd = new PiaDataDope();

            //一维数组转二维数组
            foreach (var dataArray in pdd.RowToObjectArray(rowList))
            {
                //输出表头
                if (isTableHeard)
                {
                    WriteTabeleHader();
                }
                OutPutRange = OutPutWorkSheet.Cells[OutPutRow, OutPutColumn];
                OutPutRange = OutPutRange.get_Resize(dataArray.GetUpperBound(0) + 1, dataArray.GetUpperBound(1) + 1);
                OutPutRange.Value = dataArray;
                OutPutRow = excelWorkSheet.Range("A1").XlDownRow + 1;
                isTableHeard = false;
            }

        }


        private static void WriteTabeleHader()
        {

            OutPutRange = OutPutWorkSheet.Cells[OutPutRow, OutPutColumn];
            OutPutRange = OutPutRange.get_Resize(1, DataBase.OracleManager.tableHeader.GetUpperBound(0) + 1);
            OutPutRange.Value = DataBase.OracleManager.tableHeader;
            OutPutRow = OutPutRow + 1;
        }


        /// <summary>
        /// 记录输出首行位置
        /// </summary>
        internal static int OutPutRow
        {
            get;
            set;
        }

        /// <summary>
        /// 记录输出首列位置
        /// </summary>
        private static int OutPutColumn
        {
            get;
            set;
        }

        /// <summary>
        /// 记录输出工作表
        /// </summary>
        private static EXCEL.Worksheet OutPutWorkSheet
        {
            get;
            set;
        }

        /// <summary>
        /// 记录输出单元格
        /// </summary>
        private static EXCEL.Range OutPutRange
        {
            get;
            set;
        }
    }
}
