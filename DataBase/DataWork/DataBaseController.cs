﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DataWork
{
    /// <summary>
    /// 数据库控制器
    /// </summary>
    public abstract class DataBaseController : IDataBaseController
    {

        #region internal Class

        protected class ConnectionContext
        {
            public string DataBase
            {
                get; set;
            }

            public string UserName
            {
                get; set;
            }

            public string Password
            {
                get; set;
            }
        }

        #endregion


        public string GetConnection()
        {
            throw new NotImplementedException();
        }

        protected abstract void SetConnectionCore(ConnectionContext context);


        public string GetProcedureName()
        {
            throw new NotImplementedException();
        }

        public object[] GetProcedureParameters()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Row> QueryByProcedure()
        {
            throw new NotImplementedException();
        }
    }
}
