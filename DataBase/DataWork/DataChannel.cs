﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DataWork
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class DataChannel : IDataChannel
    {
        public abstract void Send(IEnumerable<Row> rows);
    }
}
