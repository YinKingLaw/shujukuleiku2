﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DataWork
{
    /// <summary>
    /// 数据库控制器
    /// </summary>
    public interface IDataBaseController
    {
        /// <summary>
        /// 数据库连接
        /// </summary>
        string GetConnection();


        /// <summary>
        /// 获取存储过程名字
        /// </summary>
        /// <returns></returns>
        string GetProcedureName();

        /// <summary>
        /// 获取存储过程的参数
        /// </summary>
        /// <returns></returns>
        object[] GetProcedureParameters();

        /// <summary>
        /// 执行存储过程以获取查询结果
        /// </summary>
        /// <returns></returns>
        IEnumerable<Row> QueryByProcedure();

    }
}
