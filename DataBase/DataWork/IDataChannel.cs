﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataBase.DataWork
{
    /// <summary>
    /// 数据传输接口
    /// </summary>
    public interface IDataChannel
    {

        /// <summary>
        /// 发送数据到目标
        /// </summary>
        /// <param name="rows">待发送的数据</param>
        void Send(IEnumerable<Row> rows);

    }
}
