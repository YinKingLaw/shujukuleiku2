﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataBase
{


    /// <summary>
    /// 数据库默认配置
    /// </summary>
    public enum DataBaseType
    {
        /// <summary>
        /// DBBG库
        /// </summary>
        Oracle,
        /// <summary>
        /// DBND库
        /// </summary>
        SQL
    }
}
