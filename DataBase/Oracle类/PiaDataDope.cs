﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataBase
{
    /// <summary>
    /// 数据库数据仓库
    /// </summary>
    public class PiaDataDope
    {
        private object[,] mArray;
        private int r = 0;
        private int mRows = 0;

        internal IEnumerable<object[,]> BatchToObjectArray(Batch batch)
        {
            OracleRow rowData = new OracleRow();
            mArray = new object[batch.RowsCount, batch.ColumnsCount];
            foreach (var item in batch.RowsList)
            {
                rowData.RowData = item;
                ObjectAdd(rowData);
            }
            yield return mArray;
        }

        /// <summary>
        /// 定义二维数组行
        /// </summary>
        public int OutPutArrRows
        {
            get
            {
                return mRows;
            }
            set
            {
                mRows = value;
                if (mRows < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }

            }
        }

        /// <summary>
        /// 行转二维数组
        /// </summary>
        /// <param name="rowlist">可枚举的行</param>
        /// <returns></returns>
        internal IEnumerable<object[,]> RowToObjectArray(IEnumerable<OracleRow> rowlist)
        {

            foreach (OracleRow row in rowlist)
            {
                if (r == 0)
                {
                    mArray = new object[mRows, row.RowData.Length];
                }

                //数组已用行数少于预设行数,添加数据
                if (r < mArray.GetUpperBound(0) + 1)
                {
                    //把行数据每个元素转为二维数组
                    ObjectAdd(row);
                }
                else
                {
                    yield return mArray;
                    r = 0;
                }
            }

            if (r < mArray.GetUpperBound(0))
            {
                yield return mArray;
            }
        }

        /// <summary>
        /// 每行元素添加至数组
        /// </summary>
        /// <param name="row"></param>
        private void ObjectAdd(OracleRow row)
        {
            int c = 0;
            foreach (var item in row.RowData)
            {
                mArray[r, c] = item;
                c++;
            }
            r++;
        }
        /// <summary>
        /// 行统计
        /// </summary>
        public int RowsCount
        {
            get;
          private  set;
        }
        /// <summary>
        /// 列统计
        /// </summary>
        public int ColumnsCount
        {
            get;
          private  set;
        }

    }
}
