﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataBase
{
    /// <summary>
    /// 批次对象
    /// </summary>
    public class Batch
    {
        private List<object[]> objectArray = new List<object[]>();

        /// <summary>
        /// 封装List object[] 类型
        /// </summary>
        internal List<object[]> RowsList
        {
            private set
            {
                objectArray = value;
            }
            get
            {
                return objectArray;
            }
        }

        /// <summary>
        /// 批次行数
        /// </summary>
        public int RowsCount
        {
            get
            {
                return RowsList.Count;
            }
        }

        /// <summary>
        ///批次列数 
        /// </summary>
        public int ColumnsCount
        {
            get
            {
                return RowsList[0].Length;
            }
        }

        /// <summary>
        /// 根据批次中指定索引返回行数据
        /// </summary>
        /// <param name="index">批次中索引</param>
        /// <returns></returns>
        public OracleRow ObjectArrayToRow(int index)
        {
            OracleRow row = new OracleRow();
            row.RowData = this.RowsList[index];
            return row;
        }
    }
}
