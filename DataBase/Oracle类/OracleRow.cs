﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataBase
{
    /// <summary>
    /// 行对象
    /// </summary>
    public class OracleRow
    {
        /// <summary>
        /// 记录行数据
        /// </summary>
        public object[] RowData
        {
            get;
            set;
        }

    }
}
